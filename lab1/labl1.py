import random

def partition(arr, low, high):
    i = (low - 1)
    pivot = arr[high]
    for j in range(low, high):
        if arr[j] < pivot:
            i = i + 1
            arr[i], arr[j] = arr[j], arr[i]
    arr[i + 1], arr[high] = arr[high], arr[i + 1]
    return (i + 1)


def quickSort(arr, low, high):
    if low < high:
        pi = partition(arr, low, high)
        quickSort(arr, low, pi - 1)
        quickSort(arr, pi + 1, high)

random_value = lambda len, a, b : [random.randint(a, b) for i in range(len)]

use_sort = lambda list_of_random : quickSort(list_of_random, 0, len(list_of_random) - 1)


random_value = random_value(10, 1, 100)
use_sort(random_value)

for i, batch in enumerate(random_value):
    print("sorted : ", batch)