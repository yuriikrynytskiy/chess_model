from importlibs import *
from abstraction import *
from collection import *

from abstraction import PalyerMove
from collection import CollectionOfGame


class Pawn(PalyerMove, CollectionOfGame):
    def __init__(self, player: str, coord, time=5):
        self.player = player
        self.__coord = coord  # only len(coord) == 2
        self.time = time
        self.player1 = collections.defaultdict(list)
        self.player2 = collections.defaultdict(list)

        '''
        Structure of saving game : 
        {"iteration : int, Player : str, coord : list}
        '''
        CollectionOfGame.__init__(self, self.player1, self.player2)

    def __new__(cls, player, coord):
        if player != "black" and player != "white":
            raise Exception("Please write right player ! ")
        elif len(coord) != 2:
            raise Exception("Please cheak len coods !")
        else:
            print("WELCOME TO GAME ! ")
            return object.__new__(cls)

    def get_available_moves(self):
        # get all moves
        moves = list([(self.__coord[0] + 1, self.__coord[1] + 0)])
        return moves

    def cheak_move(self, move_to, all_moves):
        if move_to and len(move_to) == 2 and move_to in all_moves:
            return True
        else:
            return False

    def func_decorator_time(function):
        def wrapper(self, *args, **kwargs):
            start = timeit.timeit()
            function(self, *args, **kwargs)
            finish = timeit.timeit()
            if start - finish > self.time:
                raise Exception("You late !")
            else:
                print("Perfect time !")

        return wrapper

    @func_decorator_time
    def move(self, iteration, move_to=None, auto_move=False):
        if auto_move:
            self.coord = [iteration, random.choice(self.get_available_moves())]
            super().save_result({"Iteration": iteration, "Coords": move_to, "Player": self.player}, self.player)
        if self.cheak_move(move_to, self.get_available_moves()):
            self.coord = move_to
            super().save_result({"Iteration": iteration, "Coords": move_to, "Player": self.player}, self.player)
        else:
            pass
            #raise Exception("HELLO NOT RIGHT MOVE !")

    def set_coord(self, args): #coord, iteration: int, ):
        self.__coord = args[1]
        super().save_result({"Iteration": args[0],  "Coords": args[1], "Player": self.player}, self.player)

    def get_coord(self, player):
        return super().get_info_moves(player)

    def delete_coord(self):
        del self.__coord

    coord = property(get_coord, set_coord, delete_coord)

# PLAYER = ['white', 'black']
# coord = (1, 2)
#
# pawn_object = pawn(PLAYER[0], coord)
#
# print(pawn_object.coord)
# #
#
# move = pawn_object.get_available_moves()
#
# print(move)
# print(type(move))
#
# pawn_object.move(move_to=(2, 2))
