from importlibs import *
from pawns import Pawn
from king import King

PLAYER = ['white', 'black']

player1Pawn = Pawn(player=PLAYER[0], coord=(0, 2))
player2Pawn = Pawn(player=PLAYER[1], coord=(0, 2))

player1Pawn.move(1, auto_move=True)
player1Pawn.move(2, auto_move=True)
player1Pawn.move(3, auto_move=True)

print(player1Pawn.get_coord(PLAYER[0]))

player2Pawn.move(1, auto_move=True)
player2Pawn.move(2, auto_move=True)
player2Pawn.move(3, auto_move=True)

print(player2Pawn.get_coord(PLAYER[1]))


